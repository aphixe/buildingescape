// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "buildingEscapeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class BUILDINGESCAPE_API AbuildingEscapeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
